# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kmail package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-03 00:47+0000\n"
"PO-Revision-Date: 2022-01-02 15:02+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: mailmergeconfiguredialog.cpp:33
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr "Configureren"

#: mailmergeconfiguredialog.cpp:50
#, kde-format
msgid "Mail Merge Agent"
msgstr "Agent voor samenvoegen in e-mail"

#: mailmergeconfiguredialog.cpp:52
#, kde-format
msgid "Merge email addresses agent."
msgstr "E-mailadressenagent samenvoegen."

#: mailmergeconfiguredialog.cpp:54
#, kde-format
msgid "Copyright (C) 2021-%1 Laurent Montel"
msgstr "Copyright (C) 2021-%1 Laurent Montel"

#: mailmergeconfiguredialog.cpp:56
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: mailmergeconfiguredialog.cpp:56
#, kde-format
msgid "Maintainer"
msgstr "Onderhouder"

#: mailmergeconfiguredialog.cpp:59
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Freek de Kruijf - 2021"

#: mailmergeconfiguredialog.cpp:59
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "freekdekruijf@kde.nl"
